from django.db import models

# Create your models here.
class Basic_info(models.Model):
    job_num = models.CharField(primary_key=True,max_length=6) #工号
    name = models.CharField(max_length=8) #姓名
    sex = models.BooleanField() #性别
    birthdate = models.DateField() #出生年月
    professional_title = models.CharField(max_length=16) #职称
    work_fields = models.CharField(max_length=10) #工作领域
    start_time = models.DateField() #工作开始时间
    company = models.CharField(max_length=40) #公司
    department = models.CharField(max_length=20) #部门
    tel = models.CharField(max_length=13) #座机电话

    def __str__(self):
        return self.name

class Achievements(models.Model):
    person = models.ForeignKey(Basic_info)
    type = models.CharField(max_length=6) #成果类型
    detail = models.CharField(max_length=8) #成果详细
    achi_name = models.CharField(max_length=80) #成果名称
    achi_date = models.DateField() #成果获取时间

    def __str__(self):
        return self.achi_name

class Prize(models.Model):
    person = models.ForeignKey(Basic_info)
    level = models.CharField(max_length=4) #获奖级别
    priz_name = models.CharField(max_length=80) #获奖名称
    priz_date = models.DateField() #获奖日期

    def __str__(self):
        return self.priz_name

class Certificate(models.Model):
    person = models.ForeignKey(Basic_info)
    cert_name = models.CharField(max_length=80) #证书名称
    cert_date = models.DateField() #证书获取时间
    cert_exp_time = models.DateField() #证书过期时间

    def __str__(self):
        return self.cert_name

class Take_part_in(models.Model):
    person = models.ForeignKey(Basic_info)
    take_level = models.CharField(max_length=4) #参加活动级别
    take_name = models.CharField(max_length=80) #参加活动名称
    take_date = models.DateField() #参加活动时间

    def __str__(self):
        return self.take_name