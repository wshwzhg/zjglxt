from django.conf.urls import url
from zjgl import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index),
    url(r'^login/$', views.login),
]