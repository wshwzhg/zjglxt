from django.shortcuts import render
from django.utils import timezone

#主页视图



def index(request):
    t = timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M:%S")
    return render(request,'index.html',{'datetime':t,'total_expert':80})

#登录页视图
def login(request):
    return render(request,'login.html')

